export default [
  { code: "USD", name: "United States Dollar", rate: 1.171351 },
  { code: "CAD", name: "Canadian Dollar", rate: 1.468113 },
  { code: "IDR", name: "Indonesian Rupiah", rate: 16898.203733 },
  { code: "GBP", name: "British Pound Sterling", rate: 0.848234 },
  { code: "CHF", name: "Swiss Franc", rate: 1.081883 },
  { code: "SGD", name: "Singapore Dollar", rate: 1.593225 },
  { code: "INR", name: "Indian Rupee", rate: 87.190749 },
  { code: "MYR", name: "Malaysian Ringgit", rate: 4.966811 },
  { code: "JPY", name: "Japanese Yen", rate: 129.768141 },
  { code: "KRW", name: "South Korean Won", rate: 1356.810787 },
]