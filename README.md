# Currency Converter

## Docker Setup

```bash
# build image
$ docker build -t currency-converter .

# run container
$ docker run --name currency-container -p 3000:3000 currency-container
```

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).

## Structures

### `layouts`

- default.vue
- error.vue

### `pages`

- index.vue

### `plugins`

- [lodash](https://lodash.com/docs/4.17.15)

### `static`

- actives.js - Default active conversion card list
- rates.js - Default latest rates from [exchangeratesapi.io](https://exchangeratesapi.io/documentation/) at 1628675163
- symbols.js - Default symbols from [exchangeratesapi.io](https://exchangeratesapi.io/documentation/) at 1628675163
